<?php require('include/header.php'); ?>
<section class="lots-list">
  <div class="container">
    <h1>Cписок добавленных лотов</h1>
    <div class="row lots-placement homepage">
      <div class="col-12 col-md-6 mb-4 lot-for-clone d-none">
        <div class="lot-item">
        <input type="hidden" id="lot-id">
        <div class="status d-flex align-items-center justify-content-center lot-title"></div>
          <div class="row">
            <div class="col-12 order-2 col-lg-10 order-lg-1">
              <h3 class="lot-title">Название лота</h3>
              <p class="lot-info">Валюта: <span class="lot-currency_name"></span> <br>Количество: <span class="lot-amount"></span> <br></p>
              <p><strong>Стоимость в рублях: <span class="lot-price"></span></strong></p>
            </div>
            <div class="col-12 order-1 col-lg-2 order-lg-2">
              <div class="lot-currency mx-auto"></div>
            </div>
            <div class="col-12 order-3 d-flex">
              <button class="btn bg-blue-lighter text-blue mr-auto btn-buy" data-status="1">Купить</button>
              <button class="btn bg-blue-lighter text-blue btn-reject" data-status="2">Отклонить</button>
            </div>
          </div>
        </div>
      </div>
      <? if (glob('lots.json')) : ?>
        <? foreach(json_decode(file_get_contents('lots.json'))->lots as $lot) : ?>
        <? if (!$lot->status) : ?>
        <div class="col-12 col-md-6 mb-4">
        <div class="lot-item" id="lot-<?= $lot->id ?>">
        <input type="hidden" id="lot-id" value="<?= $lot->id ?>">
        <div class="status d-flex align-items-center justify-content-center lot-title" <?= $lot->status > 0 ? 'style="z-index: 5; opacity: 1;"' : '' ?>> <?= $lot->status ? $status[$lot->status] : '' ?></div>
          <div class="row">
            <div class="col-12 order-2 col-lg-10 order-lg-1">
              <h3 class="lot-title"><?= $lot->title ?></h3>
              <p class="lot-info">Валюта: <span class="lot-currency_name"><?= $lot->currency_name ?></span> <br>Количество: <span class="lot-amount"><?= $lot->amount ?></span> <br></p>
              <p><strong>Стоимость в рублях: <span class="lot-price"><?= $lot->price ?></span></strong></p>
            </div>
            <div class="col-12 order-1 col-lg-2 order-lg-2">
              <div class="lot-currency mx-auto"><?= $lot->currency ?></div>
            </div>
            <div class="col-12 order-3 d-flex">
              <button class="btn bg-blue-lighter text-blue mr-auto btn-buy" data-id="<?= $lot->id ?>" data-status="1">Купить</button>
              <button class="btn bg-blue-lighter text-blue btn-reject" data-id="<?= $lot->id ?>" data-status="2">Отклонить</button>
            </div>
          </div>
        </div>
      </div>
          <? endif; ?>
        <? endforeach; ?>
      <? endif; ?>
    </div>
  </div>
</section>
<?php require('include/footer.php'); ?>