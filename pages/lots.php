<?php require('include/header.php'); ?>
<section class="lots">
  <div class="container">
    <div class="form-container">
      <div class="form-content">
        <h2 class="form-title">Создать лот</h2>
        <form action="/?action=add" method="post" id="add_lot">
        <input type="hidden" name="action" value="add">
          <div class="row mb-4">
            <div class="col-12 col-md-6">
              <div class="custom-input">
                <input type="text" name="title" id="title" required>
                <div class="placeholder">Название лота</div>
              </div>
            </div>
            <div class="col-12 col-md-6">
              <div class="custom-input">
                <input type="number" name="amount" id="amount" required>
                <div class="placeholder">Сумма</div>
              </div>
            </div>
          </div>
          <label>Валюта</label>
          <div class="btn-group btn-group-toggle w-100 mb-4" data-toggle="buttons">
            <label class="btn btn-currency active">
              <input type="radio" name="currency"  value="JPY" autocomplete="off" checked> <span>Юань</span>
            </label>
            <label class="btn btn-currency">
              <input type="radio" name="currency"  value="CNY" autocomplete="off"> <span>Йена</span>
            </label>
            <label class="btn btn-currency">
              <input type="radio" name="currency"  value="CHF" autocomplete="off"> <span>Швейцарский франк</span>
            </label>
          </div>
          <button class="btn btn-submit btn-lg bg-yellow text-black mr-3" type="submit">Создать лот</button><span id="message"></span>
        </form>
      </div>
    </div>
  </div>
</section>

<section class="lots-list">
  <div class="container">
    <div class="row lots-placement lots-editor">
      <div class="col-12 col-md-6 mb-4 lot-for-clone d-none">
        <div class="lot-item">
        <div class="status d-flex align-items-center justify-content-center lot-title"></div>
          <div class="row">
            <div class="col-12 order-2 col-lg-10 order-lg-1">
              <h3 class="lot-title">Название лота</h3>
              <p class="lot-info">Валюта: <span class="lot-currency_name"></span> <br>Количество: <span class="lot-amount"></span> <br></p>
              <p><strong>Стоимость в рублях: <span class="lot-price"></span></strong></p>
            </div>
            <div class="col-12 order-1 col-lg-2 order-lg-2">
              <div class="lot-currency mx-auto"></div>
            </div>
          </div>
        </div>
      </div>
      <? if (glob('lots.json')) : ?>
        <? foreach(json_decode(file_get_contents('lots.json'))->lots as $lot) : ?>
        <div class="col-12 col-md-6 mb-4">
        <div class="lot-item" id="lot-<?= $lot->id ?>">
        <div class="status d-flex align-items-center justify-content-center lot-title" <?= $lot->status > 0 ? 'style="z-index: 5; opacity: 1;"' : '' ?>> <?= $lot->status ? $status[$lot->status] : '' ?></div>
          <div class="row">
            <div class="col-12 order-2 col-lg-10 order-lg-1">
              <h3 class="lot-title"><?= $lot->title ?></h3>
              <p class="lot-info">Валюта: <span class="lot-currency_name"><?= $lot->currency_name ?></span> <br>Количество: <span class="lot-amount"><?= $lot->amount ?></span> <br></p>
              <p><strong>Стоимость в рублях: <span class="lot-price"><?= $lot->price ?></span></strong></p>
            </div>
            <div class="col-12 order-1 col-lg-2 order-lg-2">
              <div class="lot-currency mx-auto"><?= $lot->currency ?></div>
            </div>
          </div>
        </div>
      </div>
        <? endforeach; ?>
      <? endif; ?>
    </div>
  </div>
</section>
<?php require('include/footer.php'); ?>