<?php

if ($_GET['action'] == 'startserver') {
  shell_exec('php server.php start -d');
  die();
}

if ($_GET['action'] == 'convert') {
  $amount = $_GET['amount'];
  $from = $_GET['from'];
  $data = json_decode(file_get_contents('https://www.cbr-xml-daily.ru/daily_json.js'));
  $curs = $data->Valute->$from->Value;
  $converted = $amount * $curs;
  echo round($converted, 2);
  die();
}

$status = [
  0 => 'Новая',
  1 => 'Продано',
  2 => 'Отменено'
];

if (strlen($_GET['page']) && glob('pages/' . $_GET['page'] . '.php')) {
  require('pages/' . $_GET['page'] . '.php');
} else {
  require('pages/homepage.php');
}