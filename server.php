<?php
require_once __DIR__ . '/vendor/autoload.php';
use Workerman\Worker;

$GLOBALS['connections'] = [];

function edit_lot($data)
{
    $lots = json_decode(file_get_contents('lots.json'));
    foreach ($lots->lots as $lot) {
      if ($lot->id == $data->id) {
        $lot->status = $data->status;
      }
    }
    $file = fopen("lots.json", "w");
    fwrite($file, json_encode($lots));
    fclose($file);
}
function add_lot($data)
{
  if (glob('lots.json')) {
    $lots = json_decode(file_get_contents('lots.json'));
    $lots->count++;
    $lots->lots[] = [
      'id' => $lots->count,
      'title' => $data->info->title,
      'currency_name' => $data->info->currency_name,
      'currency' => $data->info->currency,
      'price' => $data->info->price,
      'amount' => $data->info->amount,
      'status' => 0
    ];
    $file = fopen("lots.json", "w");
    fwrite($file, json_encode($lots));
    fclose($file);

    return $lots->count;
  } else {
    $lots = [
      'count' => 1,
      'lots' => [
        0 => [
          'id' => 1,
          'title' => $data->info->title,
          'currency_name' => $data->info->currency_name,
          'currency' => $data->info->currency,
          'price' => $data->info->price,
          'amount' => $data->info->amount,
          'status' => 0
        ]
      ]
    ];

    $file = fopen("lots.json", "w");
    fwrite($file, json_encode($lots));
    fclose($file);

    return 1;
  }
}

// Create a Websocket server
$ws_worker = new Worker("websocket://0.0.0.0:2346");

// 4 processes
$ws_worker->count = 4;

// Emitted when new connection come
$ws_worker->onConnect = function($connection)
{
        // Добавляем соединение в список
        $GLOBALS['connections'][] = $connection;

      //   foreach ($GLOBALS['connections'] as $c) {
      //     $c->send(json_encode(['action' => 'message', 'text' => 'user'.$c->id.' is connected. length - '.count($GLOBALS['connections'])]));
      // }
 };

// Emitted when data received
$ws_worker->onMessage = function($connection, $data)
{
  $obj = json_decode($data);
  if ($obj->action == 'add') {
    $id = add_lot($obj);
     $obj->id = 'lot-'.$id;
     $obj->info->id = $id;
  }
  if ($obj->action == 'edit') edit_lot($obj);
  // if ($obj->action == 'add') $connection->send(json_encode(['action' => 'message', 'text' => $obj->info->title]));
  foreach ($GLOBALS['connections'] as $c) {
    $c->send(json_encode($obj));
}
};

// Emitted when connection closed
$ws_worker->onClose = function($connection)
{
    echo "Connection closed\n";
};

// Run worker
Worker::runAll();