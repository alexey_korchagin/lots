var socket;

const socketMessageListener = function (event) {
  let obj = JSON.parse(event.data);
// console.log(obj);

  if (obj.action == 'add') {
    let $placement = $('.lots-placement'),
      $newLot = $('.lot-for-clone').clone(true).removeClass('lot-for-clone d-none');

    $.each(obj.info, function (key, val) {
      $newLot.find('.lot-' + key).text(val);
    });

    $newLot.find('.lot-item').attr('id', obj.id).find('#lot-id').val(obj.info.id);

    $newLot.appendTo($placement);
  } else if (obj.action == 'edit') {
    let $placement = $('.lots-placement'),
      $lot = $('#lot-' + obj.id);

    if (obj.status == 1) {
      $lot.find('.status')
      .text('Продано');
    } else {
      $lot.find('.status')
      .text('Отменено');
    }
      $lot.find('.status')
        .css({
          zIndex: 5,
          opacity: 1
        });
      if ($placement.hasClass('homepage')) {
        setTimeout(function () {
          $lot.parent().remove();
        }, 5000);
      }
  } else if (obj.action == 'message') {
    console.log(obj.text);

  }
};

const socketOpenListener = function (event) {
  console.log('Connected');
};

const socketCloseListener = function (event) {
  if (socket) {
    console.error('Disconnected.');
  }
    socket = new WebSocket('ws://127.0.0.1:2346');
  socket.addEventListener('open', socketOpenListener);
  socket.addEventListener('message', socketMessageListener);
  socket.addEventListener('close', socketCloseListener);

if (socket.readyState != 1) {
  $.get({
    url: window.location.origin,
    data: { action: 'startserver' },
  });
  socket = new WebSocket("ws://127.0.0.1:2346");
}

};

socketCloseListener();

$(document).ready(function () {


  if ($('.custom-input').length) {
    $('.custom-input input').on('keyup change', function (event) {

      if ($(event.target).val().length > 0) {
        $(event.target).closest('.custom-input').addClass('not-empty');
      } else {
        $(event.target).closest('.custom-input').removeClass('not-empty');
      }
    });
    if ($('.custom-input input').val().length > 0) {
      $('.custom-input input').closest('.custom-input').addClass('not-empty');
    }
  }
  if ($('#add_lot').length) {
    let $form = $('#add_lot');
    $form.on('submit', function (event) {
      event.preventDefault();

      let action = {
        'action': 'add',
        'info': {
          'title': $($form).find('#title').val(),
          'amount': $($form).find('#amount').val(),
          'currency': $($form).find('.btn-currency.active input').val(),
          'currency_name': $($form).find('.btn-currency.active span').text(),
        }
      };

      $.ajax({
        type: "get",
        url: window.location.origin,
        data: { action: 'convert', amount: action.info.amount, from: action.info.currency },
        success: function (response) {
          action.info.price = response;
          socket.send(JSON.stringify(action));
        }
      });


    });
  }
  $('.lots-placement ').on('click','.btn-buy, .btn-reject', function () {
    let action = {
      'action': 'edit',
      'id': $(this).closest('.lot-item').find('#lot-id').val(),
      'status': $(this).data('status')
    };

    socket.send(JSON.stringify(action));
  });
});