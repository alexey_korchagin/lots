var gulp = require('gulp'),
	autoprefixer = require('gulp-autoprefixer'),
	minify = require('gulp-clean-css'),
	sass = require('gulp-sass'),
	concatCss = require('gulp-concat-css'),
	minify_js = require('gulp-minify'),
	concat = require('gulp-concat');

gulp.task('default', ['watch_scss']);

gulp.task('watch_scss', function () {
	gulp.watch('assets/scss/*.scss', ['build_style_min']);
});
gulp.task('watch_min_js', function () {
	gulp.watch(['assets/js/*.js','!assets/js/*.min.js','!assets/js/all.js'], ['build_min_js']);
});

gulp.task('build_style_min', function () {
	return gulp.src('assets/scss/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(concatCss("style.min.css"))
		.pipe(autoprefixer({
			browsers: ['last 15 versions'],
			cascade: false
		}))
		.pipe(minify({ compatibility: 'ie8' }))
		.pipe(gulp.dest('assets/css/'));
});

gulp.task('build_min_js', function () {
	return gulp.src('assets/js/*.js')
		.pipe(minify_js({
			ext: {
				min: '.min.js'
			},
			exclude: ['tasks'],
			ignoreFiles: ['.combo.js', '-min.js', '*.min.js', 'all.js']
		}))
    .pipe(gulp.dest('assets/js'));
});

gulp.task('build_all_js', function() {
	// gulp.src('assets/js/*.min.js')
  gulp.src(['assets/js/jquery.min.js', 'assets/js/bootstrap.bundle.min.js', 'assets/js/scripts.min.js'])
    .pipe(concat('all.js'))
    .pipe(gulp.dest('assets/js'))
});